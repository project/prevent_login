<?php

namespace Drupal\prevent_login\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\prevent_login\Routing
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    $disabledRoutes = [
      'user.login',
      'user.password',
    ];

    foreach ($disabledRoutes as $routeName) {
      if ($route = $collection->get($routeName)) {
        $route->setRequirement('_access', 'FALSE');
      }
    }
  }

}
