INTRODUCTION
------------
The Prevent Login module prevents access to Drupal's login functionality.

This can be useful if authentication is being managed by external systems.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent access to Drupal's login form.

To restore access, disable the module and clear caches.
